﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectBodyCollision : MonoBehaviour {

    private Color m_MyColor;
    public Transform m_creaturePrefab;

    // Use this for initialization
    void Start () {
        m_MyColor = GetComponent<SpriteRenderer>().color;
        Debug.Log("m_MyColor = " + m_MyColor);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    //void OnCollisionStay(Collision collision) {
    //    foreach (ContactPoint contact in collision.contacts) {
    //        print(contact.thisCollider.name + " hit " + contact.otherCollider.name);
    //        Debug.DrawRay(contact.point, contact.normal, Color.white);
    //    }
    //}

    private void OnCollisionEnter2D(Collision2D collision) {
        Creature otherCreature = collision.collider.GetComponent<Creature>();

        if (otherCreature != null) {
            foreach(ContactPoint2D point in collision.contacts) {
                Debug.Log("point.normal = " + point.normal);
                Debug.DrawLine(point.point, point.point + point.normal, Color.red, 10);

                if (point.normal.y >= 0.9f) {
                    m_MyColor = otherCreature.gameObject.transform.GetComponent<SpriteRenderer>().color;
                    //m_MyColor = otherCreature.gameObject.transform.GetComponent<SpriteRenderer>().color;
                    //gameObject.GetComponent<SpriteRenderer>().color = m_MyColor;
                } else {
                    /// change color of other creature
                    otherCreature.GetComponent<SpriteRenderer>().color = m_MyColor;
                }
            }
        }
    }

    //private void OnCollisionEnter2D(Collision2D other) {
    //    Debug.Log(other.gameObject.name);
    //    Debug.Log(other.gameObject.tag);
    //    Debug.Log(other.gameObject.name);
    //    if (other.gameObject.tag == "Creature_Head") /*&& other.gameObject.transform.parent.name != gameObject.name*/
    //    {
    //        Debug.Log("Show me Da Way");
    //        m_MyColor = other.gameObject.transform.parent.GetComponent<SpriteRenderer>().color;
    //        gameObject.GetComponent<SpriteRenderer>().color = m_MyColor;
    //    }
    //}

    //private void OnCollisionEnter(Collision collision) {
    //    if (collision.gameObject.CompareTag("AI_Creature")) {
    //        Debug.Log("I collided!!!");
    //        collision.gameObject.GetComponent<Renderer>().material.color = m_MyColor.color;
    //    }
    //}
}
