﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Custom2DSimplePlayer : MonoBehaviour {
    public int speed = 10;
    public float torque;

    Rigidbody m_rigidbody;
    Vector3 velocity;

    private Animator anim;

    // Use this for initialization
    void Start() {
        m_rigidbody = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {
        velocity = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized * speed;
        float turn = Input.GetAxis("Horizontal");
        m_rigidbody.AddTorque(transform.up * torque * turn);
        m_rigidbody.AddForce(m_rigidbody.position + velocity * Time.fixedDeltaTime);
    }

    private void FixedUpdate() {
        if(velocity.x > 0){
            if(velocity.z > 0){
                anim.SetInteger("Direction", 1);
            }else if(velocity.z<0){
                anim.SetInteger("Direction", 3);
            }else{
                anim.SetInteger("Direction", 2);
            }
        }else if(velocity.x < 0){
            if(velocity.z > 0){
                anim.SetInteger("Direction", 7);
            }else if(velocity.z<0){
                anim.SetInteger("Direction", 5);
            }else{
                anim.SetInteger("Direction", 6);
            }
        }else{
            if(velocity.z > 0){
                anim.SetInteger("Direction", 0);
            }else if(velocity.z < 0){
                anim.SetInteger("Direction", 4);
            }
        }
    }
}
