﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Proto : Creature {

    [SerializeField] private float _speed = 10f;

    private Rigidbody2D rb;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
	}

    // Update is called once per frame
    void Update() {
        Movement();
    }

    private void Movement() {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        //transform.Translate(new Vector3(horizontalInput, verticalInput) * Time.deltaTime * _speed);
        transform.position += new Vector3(horizontalInput, verticalInput) * Time.deltaTime * _speed;

        if (horizontalInput > 0) {
            if (verticalInput > 0) {
                transform.localEulerAngles = new Vector3(0f, 0f, -45f);
            } else if (verticalInput < 0) {
                transform.localEulerAngles = new Vector3(0f, 0f, -135f);
            } else {
                transform.localEulerAngles = new Vector3(0f, 0f, -90f);
            }
        } else if (horizontalInput < 0) {
            if (verticalInput > 0) {
                transform.localEulerAngles = new Vector3(0f, 0f, 45f);
            } else if (verticalInput < 0) {
                transform.localEulerAngles = new Vector3(0f, 0f, 135f);
            } else {
                transform.localEulerAngles = new Vector3(0f, 0f, 90f);
            }
        } else {
            if (verticalInput > 0) {
                transform.localEulerAngles = new Vector3(0f, 0f, 0f);
            } else if (verticalInput < 0) {
                transform.localEulerAngles = new Vector3(0f, 0f, 180f);
            }
        }
    }
}
