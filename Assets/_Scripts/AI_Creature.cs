﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Creature : Creature {

    [SerializeField] EdgeCollider2D m_edgeCollider2D;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionStay2D(Collision2D collision) {
        if (collision.otherCollider == m_edgeCollider2D) {
            Debug.Log("Put code here to turn around.");
        }
    }
}
