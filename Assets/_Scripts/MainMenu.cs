﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	private CanvasGroup fadeGroup;
	private float fadeInSpeed = 0.33f;

	public Transform settingsPanel;
	private Vector3 desiredMenuPosition;

	public RectTransform menuContainer;

	// Use this for initialization
	void Start () {
		fadeGroup = FindObjectOfType<CanvasGroup>();
		fadeGroup.alpha = 1;

		InitSettings();
	}
	
	// Update is called once per frame
	void Update () {
		//fadeIn
		fadeGroup.alpha = 1 - Time.timeSinceLevelLoad * fadeInSpeed;

		menuContainer.anchoredPosition3D = Vector3.Lerp(menuContainer.anchoredPosition3D, desiredMenuPosition, 0.1f);
	}

	public void OnPlayClick(){
		SceneManager.LoadScene("Main_Game");
	}

	public void OnSettingsClick(){
		NavigateTo(1);
	}

	public void OnBackClick(){
		NavigateTo(0);
	}

	private void NavigateTo(int menuIndex){
		switch(menuIndex){
			default:
			case 0:
				desiredMenuPosition = Vector3.zero;
				break;
			case 1:
				desiredMenuPosition = Vector3.right*1919;
				break;
			case 2:
				desiredMenuPosition = Vector3.left*1919;
				break;
		}
	}

	private void InitSettings(){
		if(settingsPanel == null){
			Debug.Log("You did not set the settings panel.");
		}

		foreach(Transform t in settingsPanel){

		}
	}
}
