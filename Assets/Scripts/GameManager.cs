﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameManager : MonoBehaviour {
    [SerializeField] GameObject m_gameHUD;

    float m_endingTime;

    [SerializeField] GameObject m_menuAndPausePanel;
    
    [SerializeField] VolumePreferences m_volumePreferences;

    bool m_gameOver = false;

    public static GameManager control;
    void Awake() {
        m_volumePreferences.LoadVolumeSettings();
    }

    // Use this for initialization
    void Start () {
        //PauseGame();
    }

    // Update is called once per frame
    void Update () {

    }

    public void StartTheGame() {
        m_gameHUD.SetActive(true);

        UnpauseGame();
    }

    public void ResetGame() {
        StartTheGame();
    }

    public void GoToMainMenu() {

    }

    public void PauseGame() {
        Time.timeScale = 0;
    }

    public void UnpauseGame() {
        Time.timeScale = 1;
    }

    public void GameOver() {
        PauseGame();

        StartCoroutine(DisplayGameOverMessage());
    }

    public IEnumerator DisplayGameOverMessage() {
        Debug.Log("Hello");
        /// Add a paused time between these 2 events.
        yield return new WaitForSecondsRealtime(3);
        Debug.Log("Hello from the other side!!!");

        yield return new WaitForSecondsRealtime(5);

        ResetTheUI();
    }

    public void ResetTheUI() {
        m_menuAndPausePanel.SetActive(false);
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

}